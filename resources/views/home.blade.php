@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-14">
            <div class="card">
                <div class="card-header">Lista de Usuarios</div>

                <div class="card-body">
                        <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Email</th>
                                <th scope="col">#</th>
                                <th scope="col">#</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($usersList as $user)
                            <tr>
                                <th scope="row"> {{ $user->id }} </th>
                                <td> {{ $user->name }}</td>
                                <td> {{ $user->email }} </td>
                                <td> 
                                    <form method="POST" action="{{ route('users.show', $user->id) }}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $user-> id }}">
                                        <button type="summit" class="btn btn-outline-secondary btn-sm" >Ver Usuario</button>
                                    </form>
                                </td>
                                <td> 
                                    <form method="POST" action="{{ route('users.edit', $user->id) }}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $user-> id }}">
                                        <button type="summit" class="btn btn-outline-dark btn-sm" >Editar Usuario</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>  
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
