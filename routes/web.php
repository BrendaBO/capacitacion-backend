<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/inicio', 'HomeController@index')->name('home');
Route::get('/users', 'UsersController@index')->name('users.index');
Route::post('/users/edit', 'UsersController@edit')->name('users.edit');
Route::post('/users/mostraUsuario', 'UsersController@show')->name('users.show');
Route::put('/users/editarUsuario', 'UsersController@update')->name('users.update');
Route::delete('users/{user}', 'UsersController@destroy')->name('users.destroy');
Route::get('fake-users/{cantidad}', function(Request $request, $cantidad){
    return factory(App\User::class, (int)$cantidad)->create();
});